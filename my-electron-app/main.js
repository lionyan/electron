const {
    app,
    BrowserWindow,
    Notification
} = require('electron')

function createWindow() {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            // 允许开发者调试工具
            devTools: true,
            // 允许 node.js 环境
            nodeIntegration: true,
        },
    })
    // 加载 index.html
    win.loadFile('index.html')
    // 打开开发者调试工具
    win.openDevTools();
}

app.whenReady().then(createWindow).then(showNotification)

app.on('window-all-closed', () => {
    if (process.platform != 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length == 0) {
        createWindow();
    }
})

function showNotification() {
    const notification = {
        title: 'Base Title',
        body: 'Main Content'
    }
    new Notification(notification).show()
}